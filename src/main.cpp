#include <slice.h>
#include <slicegraphs.h>

waveGraph* mic_graph;

float hue = 0;
void RecvAudio (void* userdata, Uint8* stream, int len)
{
    float* samples = (float*)stream;
    len /= sizeof(*samples);
    waveAppendSamples(mic_graph,samples,len);
    float avg_mag = 0;
    for (int i = 0; i < len; i++)
    {
        avg_mag += fabsf(samples[i]);

    }
    avg_mag /= 4096;
    slHSVA hsva = {slClamp255(avg_mag * 4),0xFF,0xFF,0xFF};
    hsva.s = hsva.h;
    hsva.v = (hsva.h >> 1) | 128;
    hsva.h = slClamp255(hue);
    hue += 0.01;
    hue = fmodf(hue,1);
    hsva.h = rand();
    mic_graph->color = slHSVAtoRGBA(hsva);
}

int main ()
{
    slInit("Mic Input Demo - Slice Game Engine");
    graphsInit();
    slGetKeyBind("Toggle Fullscreen",slKeyCode(SDLK_f))->onpress = slToggleFullscreen;

    SDL_AudioSpec microphone_spec;
    microphone_spec.freq = 48000;
    microphone_spec.format = AUDIO_F32;
    microphone_spec.channels = 1;
    microphone_spec.samples = 4096;
    microphone_spec.callback = RecvAudio;

    slBox* fail_info = slCreateBox(txtRenderMultiline("Can't detect microphone.\n\nPress the space bar to check again."));
    fail_info->SetDims(0.25,0.5,100);

    while (!slGetReqt())
    {
        SDL_AudioDeviceID mic = SDL_OpenAudioDevice(NULL,SDL_TRUE,&microphone_spec,&microphone_spec,0);
        if (fail_info->visible = !mic)
        {
            slCycle();
            continue;
        }

        mic_graph = waveCreateGraph(12000,.25);
        waveSetGraphDims(mic_graph,0,1,100);
        mic_graph->color = {0xFF,0xFF,0xFF,0xFF};

        SDL_PauseAudioDevice(mic,0);

        while (!slGetReqt())
        {
            if (SDL_GetAudioDeviceStatus(mic) != SDL_AUDIO_PLAYING) break;
            slCycle();
        }

        SDL_CloseAudioDevice(mic);

        waveDestroyGraph(mic_graph);
    }

    slDestroyBox(fail_info);

    graphsQuit();
    slQuit();
}
