#!/bin/sh
set -eux
rm -rf built
#docker pull glassblade/slice:ubuntu-debug
docker build . -f builders/ubuntu/Dockerfile -t mic-demo-ubuntu --build-arg DEBUG="-debug"
docker create --name mic-demo-ubuntu mic-demo-ubuntu
docker cp mic-demo-ubuntu:/game/built built
docker rm mic-demo-ubuntu
cd built
./mic-demo
