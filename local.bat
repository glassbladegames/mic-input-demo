rmdir /s /q built
#docker pull glassblade/slice:windows-debug
docker build . -f builders/windows/Dockerfile -t mic-demo-windows --build-arg DEBUG="-debug"
if %errorlevel% neq 0 exit /b %errorlevel%
docker create --name mic-demo-windows mic-demo-windows
docker cp mic-demo-windows:/game/built built
docker rm mic-demo-windows
cd built
mic-demo.exe
